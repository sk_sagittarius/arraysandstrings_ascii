﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArraysAndStrings_ASCII
{
    class Program
    {
        public static void KeyCatch(int key)
        {

            if (key > 96 && key < 123)
            {
                key -= 32;
                Console.WriteLine(" " + (char)key);

                key = Console.ReadKey().KeyChar;

            }

            else if (key > 64 && key < 91)
            {
                key += 32;
                Console.WriteLine(" " + (char)key);
                key = Console.ReadKey().KeyChar;
            }

            // рекурсия, которая приводит к переполнению памяти. не стала ставить счетчик, меню или catch :)
            if (key != 32)
            {
                KeyCatch(key);
            }
            else return;

        }
        static void Main(string[] args)
        {
            Console.WriteLine("Введите буквы(для выхода нажмите пробел):");
            int key = Console.ReadKey().KeyChar;
            KeyCatch(key);
        }
    }
}
